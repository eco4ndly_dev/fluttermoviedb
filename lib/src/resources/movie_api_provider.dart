import 'dart:async';
import 'package:http/http.dart' show Client;
import 'dart:convert';
import '../models/item_model.dart';

class MovieApiProvider {
  Client client = Client();
  final api_key = "api_key";

  Future<ItemModel> fetchMovieList() async {
    print("Entered");
    final response = await client.get("http://www.mocky.io/v2/5d5a3b912f0000850436f42b");
    print(response.body.toString());
    if(response.statusCode == 200) {
      return ItemModel.fromJson(json.decode(response.body));
    } else {
      throw Exception("Unable to fetch data");
    }
  }
}