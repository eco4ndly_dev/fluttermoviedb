import 'package:movie_db/src/models/item_model.dart';

import 'movie_api_provider.dart';

class Repository {
  final movieProviderApi = MovieApiProvider();
  Future<ItemModel> fetchAllMovies() => movieProviderApi.fetchMovieList();
}